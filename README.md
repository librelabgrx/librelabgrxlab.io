# Blog provisional de LibreLabGRX
### https://librelabgrx.cc

Grupo para la difusión y el apoyo al software/hardware libre y la cultura abierta en Granada.

Basado en [**Alain's Tech Blog**](https://github.com/alainpham/alainpham.github.io) de [Alain Pham](https://github.com/alainpham).
